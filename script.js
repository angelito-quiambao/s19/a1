
	//3
	const getCube = 2 ** 3;
	//4
	console.log(`The cube of 2 is ${getCube}`);

	//5
	const address = ["258 Washington Ave NW", "California", "90011"];
	//6
	let printAddress = addDetails => {
		const [street, state, zipCode] = addDetails;
		console.log(`I live at ${street}, ${state} ${zipCode}`)
	}

	printAddress(address);

	//7
	const animal ={
		name: "Lolong",
		specie: "saltwater crodile",
		weight: "1075 kg",
		height: "20 ft 3 in"
	}
	//8
	const {name, specie, weight, height} = animal
	console.log(`${name} was a ${specie}. He weighed at ${weight} with a measurement of ${height}.`)

	//9
	let arrNumbers = [1, 2, 3, 4, 5];
	//10
	arrNumbers.forEach(numbers => console.log(numbers));
	//11
	let reduceNumber = arrNumbers.reduce((a,b) => a + b)
	console.log(reduceNumber);

	//12
	class Dog{
		constructor(name, age, breed){
			this.name = name;
			this.age = age;
			this.breed = breed;
		}
	}

	//13
	let dog1 = new Dog("Frankie", 5, "Miniature Dachshund");
	console.log(dog1);